import sys
sys.path.append("/Users/rahul/recommendation_engine")

import main
import unittest
import json

class MainTestCase(unittest.TestCase): 
  # more tests to be added 
  def test_preprocess(self):
    start_time = 1444176000
    end_time = 1444262400
  
    fi=open('fixture/input/sample_data.json','r')
    train_fi=open('fixture/output/sample_train_data.json','w')
    test_fi=open('fixture/output/sample_test_data.json','w')

    main.preprocess(fi, train_fi, test_fi)
    main.close_files([fi, train_fi, test_fi])

    train_fi=open('fixture/output/sample_train_data.json','r')
    test_fi=open('fixture/output/sample_test_data.json','r')

    # all timestamp for test to be in (start_time, end_time)
    lines_in_test=0
    for line in test_fi:
      rec = json.loads(line)
      lines_in_test +=1
      for timestamp in rec['event_time_list']:
        self.assertGreaterEqual(timestamp, start_time)
        self.assertLess(timestamp, end_time)

    self.assertEqual(lines_in_test, 1)

    lines_in_train=0
    for line in train_fi:
      rec = json.loads(line)
      lines_in_train +=1
      for timestamp in rec['event_time_list']:
        self.assertTrue(timestamp < start_time or timestamp >end_time)
    
    # number of lines for train to be 10 and test to be 1     
    self.assertEqual(lines_in_train, 10)
    main.close_files([train_fi, test_fi])
    
  # more tests to be added 
  def test_generate_random_product_recommendation(self):
    train_fi=open('fixture/output/sample_train_data.json','r')
    random_recommendation_fi=open('fixture/output/random_recommendation.json','w')
    main.generate_random_product_recommendation(train_fi, random_recommendation_fi)
    main.close_files([train_fi, random_recommendation_fi])

    random_recommendation_fi=open('fixture/output/random_recommendation.json','r')

    count=0
    for line in random_recommendation_fi:
      count +=1
      rec = json.loads(line)
      self.assertEqual(len(set(rec['recommendations'])), 10)
      self.assertFalse(rec['product_id'] in set(rec['recommendations']))
    
    self.assertEqual(count, 19)

  # more tests to be added 
  def test_evaluate_recommendation(self):
    test_fi=open('fixture/output/sample_test_data.json','r')
    recommendation_fi=open('fixture/output/random_recommendation.json','r')
    score = main.evaluate_recommendation(test_fi, recommendation_fi)
    main.close_files([test_fi, recommendation_fi])

    self.assertEqual(score, 0.0)

if __name__ == "__main__":
  unittest.main()
