##### DATA ##### 
Input File: user_views.json
Input file format: 
Each line is a json record. Each record contains 3 fields:
- 'uuid' : Integer. Unique user id.
- 'pid_list' : List of integers. List of product page ids viewed by the user. 
- 'event_time_list' : List of time stamps (in UTC). There is no guarantee that times are sorted chronologically.
The timestamp and pid lists are in sync. That is, event_time_list[i] is time stamp for pid_list[i] view event.

Description of the input. 
These are product page view events for some website from 2015-08-09 00:00:00 UTC to 2015-10-07 23:59:59 UTC
We have already grouped the events per user. So that each row is a unique user, and user's page view events, and time of event.


Here is a sample Python program to read json records from the file. You can work in your favorite programming language.
import json
from datetime import datetime
fi=open('user_views.json','r')
view_count = 0
min_time = 999999999999999999999
max_time = 0
for line in fi:
    rec = json.loads(line)
    view_count += len(rec['pid_list'])
    min_time = min(rec['event_time_list']+[min_time])
    max_time = max(rec['event_time_list']+[max_time])

print 'Total page views', view_count
print 'Min date', datetime.utcfromtimestamp(min_time).strftime('%Y-%m-%d %H:%M:%S')
print 'Max date', datetime.utcfromtimestamp(max_time).strftime('%Y-%m-%d %H:%M:%S')

##### TASK #####
In short - Generate and evaluate item-item similarity based recommendation for each product.
Details - item-item similarity is a Collaborative filtering technique, where you attempt to find similarity between items 
based on users' ratings of those items.
E.g. If user 1 viewed - 4,3,1,2. user 2 viewed - 3,5,6,4.
Then one can deduce following top 2 recommendations for each product:
1 -> any two of 2/3/4.
2 -> any two of 1/3/4
3 -> 4 and any of 5/6
4 -> 3 and any of 5/6
5 -> any two of 4/5/6
6 -> any two of 3/4/5

If you notice, 3 and 4 co-occur twice. So they are more similar to each other than other products.
Two products are counted to co-occur once, each time they appear at least once in a given users view history. 
E.g. if a user views 1,2,3. (1,2), (2,3), (1,3) co-occurrence counts are 1. Also, if the view history was 1,2,2,2,3 - it will still be the same. 
Co-occurrence count for each product pair is incremented only if they appear together in another users view history.

1. Split the data set in 2 parts. Test, and Training.
   Test Set - All events for the day 2015-10-07 (i.e. 2015-10-07 00:00:00 to 2015-10-07 23:59:59 UTC) 
   Training Set - All other events until 2015-10-06 23:59:59 UTC
2. From the training set, for each product, recommend 10 products randomly.
3. From the test set - for each co-occuring pair (a,b) - see if 'b' was in recommended list of 'a'. Remember, for every user, (a,b) and (b,a) both will count as 1 co-occurence each. 
 Report the % of co-occurences found in the predicted recommendations.

4. Assuming that you are asked to write an algorithm with best chance of predictions on unseen test set. How would you go about using training data to choose best model. 3-4 sentences of description is enough.
5. Any other evaluation mechanism you can think of for such data.

#### Bonus ####
6. Use any item-item recommendation algorithm to generate better than random recommendation. 
WARNING - Bonus section may take many more hours than we agreed on.


#### Hints for faster development.
When working on bonus section 
- Start with smaller subset of data to get the basic pipeline in place, then only use full data.
- Feel free to use Google, even for re-using code. Make sure to leave references to the resources/codes you end up using. 
