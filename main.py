import random
import json
from datetime import datetime
from collections import defaultdict
import time
import operator
 
import pandas as pd
from scipy.spatial.distance import cosine
from lshash import LSHash
import operator
from operator import itemgetter, attrgetter, methodcaller
# Test data will be from 1444176000 to 1444262400


def preprocess(fi, train_fi, test_fi):
  '''
  preprocess method takes input file object 
  and splits into train file and test file
  '''
  start_time = 1444176000
  end_time = 1444262400
  
  count=0
  for line in fi:
    rec = json.loads(line)
    # split the record into two parts train and test; and write to file 
    train_dict = {'uuid': rec['uuid'],
    'event_time_list': [],
    'pid_list': []}

    test_dict = {'uuid': rec['uuid'],
    'event_time_list': [],
    'pid_list': []}

    for timestamp_index in range(len(rec['event_time_list'])):
      if rec['event_time_list'][timestamp_index] > start_time and rec['event_time_list'][timestamp_index] < end_time:
        test_dict['event_time_list'].append(rec['event_time_list'][timestamp_index])
        test_dict['pid_list'].append(rec['pid_list'][timestamp_index])
      else:
        train_dict['event_time_list'].append(rec['event_time_list'][timestamp_index])
        train_dict['pid_list'].append(rec['pid_list'][timestamp_index])

    if len(train_dict['event_time_list'])!=0:
      print >> train_fi, json.dumps(train_dict)
    if len(test_dict['event_time_list'])!=0:
      print >> test_fi, json.dumps(test_dict)
    
    count = count+1
    if(count %100000 ==0): print "preprocessing %d th row" %count

def generate_random_product_recommendation(train_fi, random_recommendation_fi):
  '''
  This method takes a file containing user and item_viewed data and 
  10 random recommendations through sampling without replacement into random_recommendation_fi
  '''
  recommendation_dict = {}

  random.seed(1234567)
  # list of documents for recommendation engine
  for line in train_fi:
    rec = json.loads(line)
    for product_id in rec['pid_list']:
      if not product_id in recommendation_dict:
        recommendation_dict[product_id] = []

  product_set = set(recommendation_dict.keys())

  for product_id in recommendation_dict:
    product_dict = {}

    # This is done to ensure that product itself is never included in
    # it's own recommendations
    product_set_for_id = product_set - {product_id} 
    product_dict['product_id'] = product_id

    # This produces random recommendation without replacement
    # This ensures that we get 10 unique recommendations
    product_dict['recommendations'] = random.sample(product_set_for_id, 10)
    print >> random_recommendation_fi, json.dumps(product_dict)


def evaluate_recommendation(test_fi, recommendation_fi):
  # from test file count co-occurences into hash_map
  print "counting co-occurence pairs"
  co_occcurence_dict = defaultdict( int )
  for line in test_fi:
    rec = json.loads(line)
    uniq_products_for_user = sorted(list(set(rec['pid_list'])))

    for product_id_index_1 in range(len(uniq_products_for_user)):
      for product_id_index_2 in range(product_id_index_1 +1, len(uniq_products_for_user)):
        instance = (uniq_products_for_user[product_id_index_1], uniq_products_for_user[product_id_index_2])
        co_occcurence_dict[instance] +=1

  # go over recommendations to get the performance
  count =0; 
  for line in recommendation_fi:
    rec = json.loads(line)
    for recommendation in rec['recommendations']:
      if (rec['product_id'], recommendation) in co_occcurence_dict or (recommendation, rec['product_id']) in co_occcurence_dict :
        count +=1
  score =0
  if len(co_occcurence_dict) >0:
    score = count*100.0/len(co_occcurence_dict) 
  return score

def close_files(file_list):
  for file_instance in file_list:
    file_instance.close()
  
def generate_lsh_item_item_cf(train_fi, recommendation_fi):

  # user_views -> item_viewed
  item_viewed_fi=open('data/output/item_viewed.json','w')
  print "generating item_viewed_dict"
  item_viewed_dict = defaultdict(list)
  max_uid=0
  for line in train_fi:
    rec = json.loads(line)
    max_uid = max(max_uid, rec['uuid'])
    for product_id in rec['pid_list']:
      item_viewed_dict[product_id].append(rec['uuid'])

  print "Writing into item_viewed_dict"
  for item in item_viewed_dict:
    record = {
      'product_id': item,
      'users_list': item_viewed_dict[item]
    }
    print >> item_viewed_fi, json.dumps(record)

  item_viewed_fi.close()

  item_viewed_fi=open('data/output/item_viewed.json','r')

  print "generating index in lsh"
  count = 0
  num_users = 10000
  max_uid = 5000000

  # index in lsh
  lsh = LSHash(6, num_users)

  print "generated lsh opbject"
  for line in item_viewed_fi:
    count += 1
    rec = json.loads(line)
    bit_map = [0]*max_uid
    if count % 100 is 0:
        print count
    for user_id in rec['users_list']:
      bit_map[user_id] =1
    lsh.index(bit_map[0:num_users], extra_data=rec['product_id'])


  print "generating recommendations"
  item_viewed_fi.close()
  item_viewed_fi=open('data/output/item_viewed.json','r')
  
  # query and write to recommendation_fi
  for line in item_viewed_fi:
    rec = json.loads(line)
    bit_map = [0]*max_uid
    for user_id in rec['users_list']:
      bit_map[user_id] =1
    recommendations = lsh.query(bit_map[0:num_users])
    
    record ={
      'product_id': rec['product_id'],
      'recommendations': get_list_of_recommendations(recommendations, product_id)
    }
    if len(record['recommendations']) >0:
      print >> recommendation_fi, json.dumps(record)

def get_list_of_recommendations(recommendations, product_id):
  rec_list = []
  # take top 10 nearest points 
  for entry in recommendations[0:10]:
    if entry[1] != 0:
      rec_list.append(entry[0][1])
  return rec_list

if __name__ == "__main__":
  start_time = time.time()
  # splitting user_views data into train and test data
  fi=open('data/input/sample_data.json','r')
  train_fi=open('data/output/train_data.json','w')
  test_fi=open('data/output/test_data.json','w')
  preprocess(fi, train_fi, test_fi)
  close_files([fi, train_fi, test_fi])

  # generating random recommendation from the train data
  train_fi=open('data/output/train_data.json','r')
  random_recommendation_fi=open('data/output/random_recommendation.json','w')
  generate_random_product_recommendation(train_fi, random_recommendation_fi)
  close_files([train_fi, random_recommendation_fi])
  
  # generating item item colaborative filtering from the train data
  train_fi=open('data/output/train_data.json','r')
  recommendation_fi=open('data/output/cf_recommendation.json','w')
  generate_lsh_item_item_cf(train_fi, recommendation_fi)
  close_files([train_fi, recommendation_fi])
  
  # evaluating random recommendation and collaborative filtering recommendation
  test_fi=open('data/output/test_data.json','r')

  random_recommendation_fi=open('data/output/random_recommendation.json','r')
  random_rec_score = evaluate_recommendation(test_fi, random_recommendation_fi)

  cf_recommendation_fi=open('data/output/cf_recommendation.json','r')
  cf_rec_score = evaluate_recommendation(test_fi, cf_recommendation_fi)
  
  print "random_generator_score: %f, cf_generator_score %f" %(random_rec_score, cf_rec_score)
  close_files([test_fi, random_recommendation_fi, cf_recommendation_fi])

  print("--- Program took %s seconds to run ---" % (time.time() - start_time))


