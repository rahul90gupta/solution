Solution Details and Implementation

Direcory Structure:
- All the source code is put in main.py
- user_views.json is to be put into data/input/
- and all outpul files including recommendation files will be generated in data/output/
- test files are in test directory and sample data for test is in fixture
- for locally-sensitive hashing, I have used a python libary lshash by kayzh

Algorithim pipeline

1. preprocess :
preprocess method puts the data between start_time and_time into test.json and rest of the data into train.json

2. generate_random_product_recommendation :
It generates random recommendations for the products (benchmark performance) through sampling without replacement

3. evaluate_recommendation:
It takes test file stream along with recommendation file and return the fraction of co-occurence in recommendation
file found from test file


4. Assuming that you are asked to write an algorithm with best chance of predictions on unseen test set. 
How would you go about using training data to choose best model. 3-4 sentences of description is enough.

Ans. Given a list of models choose the best model using training data 
1) Divide the data into three parts training(60%), cross-validation(20%) and test(20%) 
2) For an individual algorithm/model, choose parameters, such that it maximizes fitness_score on cross-validation data
3) For all the optimum model/algorithm preesent, choose the final model which has maximum fitness_score on test data 

To improve the model further:
- we can plot a graph of error for training set and cross-validation set v/s number of number of users. This
will help us know whether are algorithm is suffering from bias or variance. 
- In case of bias => we should find a model/algorithm with rich features
- In case of variance => we should train on more data

5. Any other evaluation mechanism you can think of for such data.
Ans. There are lot of mechanisms: each with their own strength and weaknesses 
1) A/B testing :- Comparing user actions on the two different recommmendation algorithms at the same time
better the ctr for recommendation, better the algorithm will be.

2) Human Judge evaluation :- Having a panel of judge user browse through recommendation results and 
- active metrics: have them rate trecommendation result for each product on a scale of 1 to 10.
- passive metrics: derive metrics like CTR etc. from judge action data to arrive at a metric

6. generate_lsh_item_item_cf:
Basic Idea of item-item collaborative filtering is calculate the neighbouhood of an item.

For, this case if an item is represented in number_of_user dimension space, problem reduces to finding 
I tried various approach to implement:
1) build the num_item x num_item matrix S, 
	such that S[i][j] = cosine-similarity of i-th item and j-th item
	Time required = greater than 10 days because Time complexity(num_item^2)
	Space Complexity = O(num_item^2) = 15k * 15 k  < 2 GB, which is manageable

	I tried replacing cosine similarity with Jaccard Similarity. It improves the algorithm a bit
	but would still take around 2 days to finish.
2) Locally-sensitive Hashing:
	for each item (num_users length bit array) 
		index the item in LSH object 
	after indexing all items, 
	
	for each item:
		get it's neighborhood, by querying the LSH object for item 
		take top 10 and that is the recommended/ similar items 

	Time Complexity: O(num_iems) => manageable, job finishes under 3 hours. parallelizing with 8 cores
	 the job would reduce time further to half an hour.
	Space Complexity: O(num_item x num_users) : it can go well over 8 GB

	Solution to space complexity problem:
		- use redis server to store the lsh index. This will increase the total time taken
		because of the disk IO
		- sample  5 % of the users and then generate the LSH index and subsequently, get the recommendations
		by querying the LSH object. and merge 
		- use bit array instead of int array for generating LSH index and querying
		- filtering out some the users to run on relatively active users
	A combination of these four fixes would make this algorithm much faster and scalable to handle much
	larger data
3) Min Hashing Solution:
	for each hash_function i:
		for each item j:
			S[i][j] = min_hash(user_set(j))
	each row corresponding to hash function will put few items into same bucket 
	combining the result over 1000 such hash functions would give a fair recommendation system
	
	
Resources used for the solution:
- http://brenocon.com/blog/2012/03/cosine-similarity-pearson-correlation-and-ols-coefficients/
- https://class.coursera.org/mmds-003/lecture/93
- https://www.quora.com/How-do-you-measure-and-evaluate-the-quality-of-recommendation-engines
- https://github.com/kayzh/LSHash
- Mining of massive datasets by Jeff Ullman, Anand Rajaraman
